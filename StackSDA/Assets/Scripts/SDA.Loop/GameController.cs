using System;
using System.Collections;
using System.Collections.Generic;
using SDA.INPUT;
using SDA.UI;
using UnityEngine;

namespace SDA.Loop
{ 
    public class GameController : MonoBehaviour
    {
        [SerializeField]
        private MainMenuView mainMenuView;

        [SerializeField]
        private StackInput gameController;


        private MainMenuState mainMenuState;
        private GameState gameState;

        private Action transitionToGameState;


        private IBaseState currentlyActiveState;


        private void Start()
        {
            transitionToGameState += () => ChangeState(gameState);

            mainMenuState = new MainMenuState(transitionToGameState, mainMenuView);
            gameState = new GameState();

            ChangeState(mainMenuState);
        }

        private void Update()
        {
            currentlyActiveState?.UpdateState();
        }

        private void OnDestroy()
        {
            
        }

        private void ChangeState(IBaseState newState)
        {
            currentlyActiveState?.DisposeState();
            currentlyActiveState = newState;
            currentlyActiveState?.InitState();
        }
    }
}
