using System;
using System.Collections;
using System.Collections.Generic;
using SDA.UI;
using UnityEngine;

namespace SDA.Loop
{
    public class MainMenuState : IBaseState
    {
        private Action transitionToGameState;
        private MainMenuView mainMenuView;

        public MainMenuState(Action transitionToGameState, MainMenuView mainMenuView)
        {
            this.transitionToGameState = transitionToGameState;
            this.mainMenuView = mainMenuView;
        }


        public void InitState()
        {
            mainMenuView.ShowView();
        }

        public void UpdateState()
        {
            Debug.Log("DUPA");
        }

        public void DisposeState()
        {
            mainMenuView.HideView();
        }
    }
}
